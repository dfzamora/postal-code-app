export interface PostalCode {
    id: number
    department: string
    municipality: string
    postalcode: string
    neighbourhood: string
}