import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { iif, of } from 'rxjs';
import { Observable } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';
import { PostalCode } from 'src/app/models/postalcodes.model';
import { PostalCodesService } from 'src/app/services/postal-codes.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  filteredCodes: Observable<PostalCode[]>;
  codesForm: FormGroup;
  loading$: Observable<boolean> = this.service.loading$;

  constructor(private fb: FormBuilder, private service: PostalCodesService) { }

  ngOnInit(): void {
    this.codesForm = this.fb.group({
      codesInput: null
    })

    this.filteredCodes = this.codesForm.get('codesInput').valueChanges
      .pipe(
        debounceTime(300),
        switchMap((value) =>
          iif(() => typeof(value) === 'string' , this.service.search({ code: value }), of(null))
        )
      );
  }

  displayFn(code: PostalCode) {
    if (code) return code.postalcode;
  }

}
