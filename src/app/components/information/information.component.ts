import { Component, Input, OnInit } from '@angular/core';
import { PostalCode } from 'src/app/models/postalcodes.model';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {

  @Input() dataInfo: PostalCode;
  constructor() { }

  ngOnInit(): void {
  }

}
