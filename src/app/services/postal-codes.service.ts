import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PostalCode } from '../models/postalcodes.model';

@Injectable({
  providedIn: 'root'
})
export class PostalCodesService {

  private readonly href: string;
  private readonly loading = new Subject<boolean>();

  constructor(private _httpClient: HttpClient) {
    this.href = environment.apiurl;
  }

  search(filter: { code: string } = { code: '' }): Observable<PostalCode[]> {
    const requestUrl = `${this.href}postalcode/${filter.code}`;

    return this._httpClient.get<PostalCode[]>(requestUrl)
      .pipe(
        tap((response: PostalCode[]) => {
          response = response
            .filter(postalcode => postalcode.postalcode.includes(filter.code))

          return response;
        })
      );
  }


  get loading$(): Observable<boolean> {
    return this.loading;
  }

}
